package ex;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrame extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static final int WIDTH = 600;
	static final int HEIGHT = 400;
	
	public MyFrame(String string) {
		super(string);
	}
	
	protected void setFrameFeatures() {
		setSize(WIDTH,  HEIGHT);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w)/2;
		int y = (dim.height - h)/2;
		setLocation(x,y);
	    setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	protected void  addComponents() {
		MyCanvas canvas = new MyCanvas();
		setContentPane(canvas);
	}
	
	public static void createAndShowGUI() {
		MyFrame window = new MyFrame("Example of how to draw on panel");
		window.addComponents();
		window.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
