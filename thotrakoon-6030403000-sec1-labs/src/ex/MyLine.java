package ex;

import java.awt.geom.Line2D;

public class MyLine extends Line2D.Double{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyLine(int x1, int x2, int y1, int y2){
		super(x1,x2,y1,y2);
	}
	
}
