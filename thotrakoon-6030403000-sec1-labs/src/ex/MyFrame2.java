package ex;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrame2 extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static final int WIDTH = 600;
	static final int HEIGHT = 400;
	
	public MyFrame2(String string) {
		super(string);
	}
	
	protected void setFrameFeatures() {
		setSize(WIDTH,  HEIGHT);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w)/2;
		int y = (dim.height - h)/2;
		setLocation(x,y);
	    setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	protected void  addComponents() {
		RunningBall canvas = new RunningBall();
		setContentPane(canvas);
	}
	
	public static void createAndShowGUI() {
		MyFrame2 window = new MyFrame2("Example of how to make ball moving");
		window.addComponents();
		window.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}