package ex;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

public class MyCanvas extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	MyLine line, line2,  line3;
	Rectangle2D.Double rect;
	
	public MyCanvas() {
		setBackground(Color.BLUE);
		line = new MyLine(0, 0, MyFrame.WIDTH, MyFrame.HEIGHT);
		line2 = new MyLine(MyFrame.WIDTH, 0, 0, MyFrame.HEIGHT);
		line3 = new MyLine(MyFrame.WIDTH/2, 0, MyFrame.WIDTH/2, MyFrame.HEIGHT);

	}
	
	public void paintComponent(Graphics g){
        super.paintComponent(g);
        
        Graphics2D g2d = (Graphics2D) g;
        
        g2d.setColor(Color.BLACK);
        g2d.draw(line);
        
        g2d.setColor(Color.RED);
        g2d.draw(line2);
        
        g2d.setColor(Color.GREEN);
        g2d.draw(line3);
	}
}
