package ex;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import javax.swing.JPanel;

public class RunningBall extends JPanel implements Runnable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Ellipse2D.Double ball,ball2;
	int velX=1, // speed on x-axis 
		velY=1; // speed on y-axis
	
	Thread running;
	
	public RunningBall() {
		ball = new Ellipse2D.Double(0, 0, 20, 20);
		ball2 = new Ellipse2D.Double(1000, 0, 20, 20);
		
		running=new Thread(this);
		running.start();
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true){	

			ball.x = ball.x + velX;
			ball.y = ball.y + velY;
			ball2.x = ball2.x - velX;
			ball2.y = ball2.y + velY;
			
			repaint();
			this.getToolkit().sync(); 
			
			// Delay
			try
			{
				
				Thread.sleep(20);
			}
			catch(InterruptedException ex)
			{
				System.err.println(ex.getStackTrace());
			}
		}
	}
	
	public void paintComponent(Graphics g){
		
		super.paintComponent(g);
		
		Graphics2D g2d = (Graphics2D) g;
		g2d.fill(ball);
		g2d.fill(ball2);

	}
}
