package lab7forlab9;

/**
 * This program PatientFormV1 extends from MySimpleWindow 
 * The program should have the following properties:
 * 1. The title of the program is set as �Patient Form V1�.
 * 2. The program should have a panel that contains components  
 * 2.1 Each line has the following properties: i)The left part contains JLabel and then
 *   ii) the right part contains JTextField which its length is 15.  
 *  Note that you need to have this value saved in a constant class member. 
 * 2.2 The first line contains the label which it is set as �Name:�
 * 2.3 The second line contains the label which it is set as �Birthdate:�  
 *  When the user places the cursor at this field, the program shows the tooltip text 
 *  as �ex. 22.02.2000�
 * 2.4 The third line contains the label which it is set as �Weight (kg.):� 
 * 2.5 The fourth line contains the label which it is set as �Height (metre)�
 * 2.6 The fifth line contains two buttons which should be named �Cancel� and �OK� respectively.
 *     
 * @author: Kanda Saikaew
 * @date:  21/02/2017
 * @version: 1.0
 */
import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;

public class AthleteFormV1 extends MySimpleWindow {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3629832942393810272L;
	protected JLabel nameLabel, dateLabel, weightLabel, heightLabel, nationalityLabel;
	protected JTextField nameTxtField, dateTxtField, weightTxtField, heightTxtField,
						 nationalityTxtField;
	protected JPanel namePanel, datePanel, weightPanel, heightPanel, nationalityPanel;
	protected JPanel overallPanel, textsPanel;
	public final static int TXTFIELD_WIDTH = 15;
	public final static int TOOLTIP_INIT_DELAY = 100;
	public final static int TOOLTIP_DISMISS_DELAY = 5000;
	
	public AthleteFormV1(String title) {
		super(title);
		 ToolTipManager.sharedInstance().setInitialDelay(TOOLTIP_INIT_DELAY);
	     ToolTipManager.sharedInstance().setDismissDelay(TOOLTIP_DISMISS_DELAY);
	}
	
	protected void initComponents() {
		namePanel = new JPanel(new GridLayout(1,2));
		datePanel = new JPanel(new GridLayout(1,2));
		weightPanel = new JPanel(new GridLayout(1,2));
		heightPanel = new JPanel(new GridLayout(1,2));
		nationalityPanel = new JPanel(new GridLayout(1,2));
		nameLabel = new JLabel("Name:");
		nameTxtField = new JTextField(TXTFIELD_WIDTH);
		dateLabel = new JLabel("Birthdate:");
		dateTxtField = new JTextField(TXTFIELD_WIDTH);
		dateTxtField.setToolTipText("ex. 22.02.2000");
		weightLabel = new JLabel("Weight (kg.):");
		weightTxtField = new JTextField(TXTFIELD_WIDTH);
		heightLabel = new JLabel("Height (metre):");
		heightTxtField = new JTextField(TXTFIELD_WIDTH);
		nationalityLabel = new JLabel("Nationality:");
		nationalityTxtField = new JTextField(TXTFIELD_WIDTH);
		textsPanel = new JPanel(new GridLayout(5,1));
		overallPanel = new JPanel(new BorderLayout());
	}
	protected void setLabelTxtField(JPanel panel,
			JLabel label, JTextField txtField) {
		panel.add(label);
		panel.add(txtField);
	}
	protected void addComponents() {
		super.addComponents();
		initComponents();
		setLabelTxtField(namePanel, nameLabel, nameTxtField);
		setLabelTxtField(datePanel, dateLabel, dateTxtField);
		setLabelTxtField(weightPanel, weightLabel, weightTxtField);
		setLabelTxtField(heightPanel, heightLabel, heightTxtField);
		setLabelTxtField(nationalityPanel, nationalityLabel, nationalityTxtField);
		textsPanel.add(namePanel);
		textsPanel.add(datePanel);
		textsPanel.add(weightPanel);
		textsPanel.add(heightPanel);
		textsPanel.add(nationalityPanel);
		overallPanel.add(textsPanel, BorderLayout.NORTH);
		overallPanel.add(buttonsPanel, BorderLayout.SOUTH);
		setContentPane(overallPanel);
	}
	
	public static void createAndShowGUI(){
		AthleteFormV1 athleteForm1 = new AthleteFormV1("Athlete Form V1");
		athleteForm1.addComponents();
		athleteForm1.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}