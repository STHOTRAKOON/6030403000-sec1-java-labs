package lab7forlab9;

/**
 * This program PatientFormV2 extends from PatientFormV1 
 * The program should have the following properties:
 * 1. The title of the program should be �Patient Form V2�
 * 2. Add the label �Gender�� and two radio buttons �Male and �Female�
 * 3. Add the label �Address:� and the text area with 2 rows and 35 columns.   
 * 4. Create the scroll pane and add the text area in that scroll pane.  
 * 5. Initialize the text area with the content as "Department of Computer Engineering, 
 * Faculty of Engineering, Khon Kaen University, Mittraparp Rd., T. Naimuang, 
 * A. Muang, Khon Kaen, Thailand, 40002"
 * 6. Place the Gender and the Address parts above the two buttons. 
 * 
 * @author: Kanda Saikaew
 * @date:  21/02/2017
 * @version: 1.0
 */
import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class AthleteFormV2 extends AthleteFormV1 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3694909354550437224L;

	protected JRadioButton maleRadioB, femaleRadioB;
	protected ButtonGroup genderGrpB;
	protected JTextArea compTxtArea;
	protected JScrollPane compScrollPane;
	protected JPanel genderPanel, compPanel, contentPanel, genderChoicesPanel;
	protected JLabel genderLabel, compLabel;
	
	public final static int NUM_TXTAREA_ROWS = 2;
	public final static int NUM_TXTAREA_COLS = 35;
	public final static String FEMALE = "Female";
	public final static String MALE = "Male";
	
	public AthleteFormV2(String title) {
		super(title);
	}
	
	protected void initComponents() {
		super.initComponents();
		maleRadioB = new JRadioButton(MALE);
		femaleRadioB = new JRadioButton(FEMALE);
		genderGrpB = new ButtonGroup();
		genderGrpB.add(maleRadioB);
		genderGrpB.add(femaleRadioB);
		maleRadioB.setSelected(true);
		genderPanel = new JPanel(new GridLayout(1,2));
		genderChoicesPanel = new JPanel();
		compTxtArea = new JTextArea(NUM_TXTAREA_ROWS, NUM_TXTAREA_COLS);
		compTxtArea.setLineWrap(true);
		compTxtArea.setWrapStyleWord(true);
		compTxtArea.setText("Competed in the 31st national championship");
		compScrollPane = new JScrollPane(compTxtArea);
		compPanel = new JPanel(new BorderLayout());
		contentPanel = new JPanel(new BorderLayout());
		genderLabel = new JLabel("Gender:");
		compLabel = new JLabel("Competition:");
	}
	
	protected void addComponents() {
		super.addComponents();
		genderChoicesPanel.add(maleRadioB);
		genderChoicesPanel.add(femaleRadioB);
		genderPanel.add(genderLabel);
		genderPanel.add(genderChoicesPanel);
		compPanel.add(compLabel, BorderLayout.NORTH);
		compPanel.add(compScrollPane, BorderLayout.SOUTH);
		contentPanel.add(textsPanel, BorderLayout.NORTH);
		contentPanel.add(genderPanel, BorderLayout.CENTER);
		contentPanel.add(compPanel, BorderLayout.SOUTH);
		overallPanel.add(contentPanel, BorderLayout.NORTH);
	}
	public static void createAndShowGUI(){
		AthleteFormV2 athleteForm2 = new AthleteFormV2("Athlete Form V2");
		athleteForm2.addComponents();
		athleteForm2.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
