package lab7forlab9;

/**
 * This program PatientFormV3 extends from PatientFormV2 
 * The program should have the following properties:
 * 1. The title of the program should be �Patient Form V3�
 * 2. Add the label �Patient type:�� and the combo box of patient types 
 *    which are �Inpatient� and �Outpatient� 
 * 3. Add the menu bar with these two following menus 
 *    3.1 Menu �File� has four menu items which are �New�, �Open�, �Save�, and �Exit�   
 *    3.2 Menu �Config� has two menu items which are �Color�, and �Size� as shown in Figure 7.
 *
 * @author: Kanda Saikaew
 * @date:  21/02/2017
 * @version: 1.0
 */
import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AthleteFormV3 extends AthleteFormV2 {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3170036318568067861L;
	protected JLabel typeLabel;
	protected JComboBox<String> typeList;
	protected JPanel typePanel;
	protected JMenuBar menuBar;
	protected JMenu fileMenu, editMenu, configMenu;
	protected JMenuItem newMI, openMI, saveMI, exitMI;
	protected JMenuItem colorMI, sizeMI;
	public AthleteFormV3(String title) {
		super(title);
	}
	
	protected void initComponents() {
		super.initComponents();
		typeLabel = new JLabel("Type:");
		typeList = new JComboBox<String>();
		typeList.addItem("Badminton player");
		typeList.addItem("Boxer");
		typeList.addItem("Footballer");
		typeList.setSelectedItem("Boxer");
		typePanel = new JPanel(new GridLayout(1,3));
	}
	
	protected void addComponents() {
		super.addComponents();
		typePanel.add(typeLabel);
		typePanel.add(typeList);
		overallPanel.add(typePanel, BorderLayout.CENTER);
	}
	
	protected void initMenus() {
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		configMenu = new JMenu("Config");
		newMI = new JMenuItem("New");
		openMI = new JMenuItem("Open");
		saveMI = new JMenuItem("Save");
		exitMI = new JMenuItem("Exit");
		colorMI = new JMenuItem("Color");
		sizeMI = new JMenuItem("Size");
	}
	
	protected void addMenus() {
		initMenus();
		fileMenu.add(newMI);
		fileMenu.add(openMI);
		fileMenu.add(saveMI);
		fileMenu.add(exitMI);
		configMenu.add(colorMI);
		configMenu.add(sizeMI);
		menuBar.add(fileMenu);
		menuBar.add(configMenu);
		setJMenuBar(menuBar);
	}
	public static void createAndShowGUI(){
		AthleteFormV3 patientForm3 = new AthleteFormV3("Patient Form V3");
		patientForm3.addComponents();
		patientForm3.addMenus();
		patientForm3.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}