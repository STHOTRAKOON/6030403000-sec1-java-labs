package surakhan.thotrakoon.lab8;

import java.awt.geom.*;

public class Ball extends Ellipse2D.Double{

	public Ball(double x, double y, double r) {
		super(x, y, 2 * r, 2 * r);
		this.r = r;
		this.x = x;
		this.y = y;
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected double x, r, y;
	

	
	public double getR() {
		return r;
	}
	public void setR(double r) {
		this.r = r;
	}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	
	

}
