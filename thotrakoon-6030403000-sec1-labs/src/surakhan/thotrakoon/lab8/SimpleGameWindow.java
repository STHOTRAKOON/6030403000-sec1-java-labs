package surakhan.thotrakoon.lab8;

import java.awt.*;
import javax.swing.*;

public class SimpleGameWindow extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int width = 650, height = 500;
	
	public SimpleGameWindow(String title) {
		super(title);
	}
	
	public static void createAndShowGUI() {
		SimpleGameWindow window = new SimpleGameWindow("Pong Game Window");
		window.addComponents();
		window.setFrameFeatures();
	}
	
private void addComponents() {
	PongPanel pong = new PongPanel();
	setContentPane(pong);
	}

private void setFrameFeatures() {
		
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(width, height);
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	
	
}
