package surakhan.thotrakoon.lab8;

import java.awt.*;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class PongPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private PongPaddle PongPaddle1,PongPaddle2;
	private int score1 = 0, score2 = 0 , r = 20
			, paddlewidth = 15, paddleheight = 80;
	private Ball ball;
	
	public PongPanel() {
		this.setBackground(Color.BLACK);
		
	}
	
	@Override
	public void paintComponent(Graphics g) {
		ball = new Ball ((getWidth() / 2) - r, (getHeight() / 2) - r, r);
		PongPaddle1 = new PongPaddle(0, (getHeight() / 2) - paddlewidth / 2, paddlewidth, paddleheight);
		PongPaddle2 = new PongPaddle(getWidth() - paddlewidth, (getHeight() / 2) - paddlewidth / 2, paddlewidth, paddleheight);
		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.WHITE);
		g2d.setFont(new Font("Serif", Font.BOLD, 48));
		g2d.drawLine(paddlewidth, 0, paddlewidth, getHeight());
		g2d.drawLine(getWidth() - paddlewidth, 0, getWidth() - paddlewidth, getHeight());
		g2d.drawLine(getWidth() / 2, 0, getWidth() / 2, getHeight());
		g2d.fill(PongPaddle1);
		g2d.fill(PongPaddle2);
		g2d.fill(ball);
		String scoreplayer1 = Integer.toString(score1);
		String scoreplayer2 = Integer.toString(score2);
		g2d.drawString(scoreplayer1, getWidth() / 4, 100);
		g2d.drawString(scoreplayer2, (getWidth() *3) / 4, 100);
		
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				
			}
		});
	}
}
