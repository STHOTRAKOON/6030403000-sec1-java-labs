package surakhan.thotrakoon.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

public class BouncyBall extends JPanel implements Runnable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Thread running;
	MovingBall ball;
	Rectangle2D.Double box;
	int r = 20;
	private int width = 650;
	private int height = 500;
	int x = (width / 2) - r;
	int y = (height / 2) - r;
	int velx = 2, vely = 2;
	
	
	
	public BouncyBall() {
		this.setBackground(Color.GREEN);
		ball = new MovingBall(x, y, r, velx, vely);
		running = new Thread(this);
		running.start();
	}
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.YELLOW);
		g2d.fill(ball);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		while(true) {
			if(ball.getX() + 50 < width && ball.getY() + 70 < height 
					&& ball.getX() > 0 && ball.getY() > 0) {
				ball.setX(ball.getX() + ball.getX1());
				ball.setY(ball.getY() + ball.getY1());
			}
			else if(ball.getX() + 50 >= width) {
				ball.setX1(-1 * velx);
				ball.setX(ball.getX() + ball.getX1());
				ball.setY(ball.getY() + ball.getY1());
			}
			else if(ball.getY() + 70 >= height) {
				ball.setY1(-1 * vely);
				ball.setX(ball.getX() + ball.getX1());
				ball.setY(ball.getY() + ball.getY1());
			}
			else if(ball.getX() <= 0) {
				ball.setX1(velx);
				ball.setX(ball.getX() + ball.getX1());
				ball.setY(ball.getY() + ball.getY1());
			}
			else if(ball.getY() <= 0) {
				ball.setY1(vely);
				ball.setX(ball.getX() + ball.getX1());
				ball.setY(ball.getY() + ball.getY1());
			}
			
			
			repaint();
			this.getToolkit().sync();
			
			try {
				
				
				Thread.sleep(5);
			}
			catch(InterruptedException ex)
			{
				System.err.println(ex.getStackTrace());
			}
		}
	}
	
}
