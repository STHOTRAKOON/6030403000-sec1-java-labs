package surakhan.thotrakoon.lab8;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class TestWrapAroundBall extends SimpleGameWindow{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TestWrapAroundBall(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	
	protected void addComponents() {
		WrapAroundBall panel = new WrapAroundBall();
		setContentPane(panel);
	}
	
	public static void createAndShowGUI() {
		TestWrapAroundBall window = 
				new TestWrapAroundBall("Test Wrap Around Ball");
		window.setFrameFeatures();
		window.addComponents();
	}
	
	private final int width = 650, height = 500;
	private void setFrameFeatures() {
		
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(width, height);
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
