package surakhan.thotrakoon.lab8;

import java.awt.*;

import javax.swing.JPanel;


public class WrapAroundBall extends JPanel implements Runnable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int width = 650;
	private int height = 500;
	private int d = 40;
	MovingBall ball1, ball2, ball3, ball4, ball5;
	private int r = 20;
	int x1 = 3, y1 = 3;
	
	Thread running;
	

		

	public WrapAroundBall() {
		this.setBackground(Color.BLUE);
		ball1 =  new MovingBall(150, 0 , r, x1 * 0, y1);
		ball2 =  new MovingBall(0, 200 , r, x1, y1 * 0);
		ball3 =  new MovingBall(0, 0 , r, x1, y1);
		ball4 =  new MovingBall(300, 300, r, x1 * 0, y1 * (-1));
		ball5 =  new MovingBall(590, 300 , r, x1 * (-1) , y1 * 0);
		
	
		running = new Thread(this);
		running.start();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
	
	while(true){	
		if(ball1.getY() <= height) {
			ball1.setX(ball1.getX() + ball1.getX1());
			ball1.setY(ball1.getY() + ball1.getY1());
		}
		else {
				ball1.setY(-d);
			}
		if(ball2.getX() <= width) {
			ball2.setX(ball2.getX() + ball2.getX1());
			ball2.setY(ball2.getY() + ball2.getY1());
		}
		else {
			ball2.setX(-d);
		}
		if(ball3.getY() <= height && ball3.getX() <= width) {
			ball3.setX(ball3.getX() + ball3.getX1());
			ball3.setY(ball3.getY() + ball3.getY1());
		}
		else if(ball3.getY() > height) {
			ball3.setX(ball3.getX() + ball3.getX1());
			ball3.setY(-d);
		}
		else if(ball3.getX() > width) {
			ball3.setX(-d);
			ball3.setY(ball3.getY() + ball3.getY1());
		}
		if(ball4.getY() >= 0 - d) {
			ball4.setX(ball4.getX() + ball4.getX1());
			ball4.setY(ball4.getY() + ball4.getY1());
		}
		else {
			ball4.setY(height + d);
		}
		if(ball5.getX() >= 0 - d) {
			ball5.setX(ball5.getX() + ball5.getX1());
			ball5.setY(ball5.getY() + ball5.getY1());
		}
		else {
			ball5.setX(width + d);
		}
		repaint();
		this.getToolkit().sync(); 
		
		// Delay
		try
		{
			
			Thread.sleep(20);
		}
		catch(InterruptedException ex)
		{
			System.err.println(ex.getStackTrace());
		}
	}	
	}

	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.YELLOW);
		g2d.fill(ball1);
		g2d.setColor(Color.RED);
		g2d.fill(ball2);
		g2d.setColor(Color.GREEN);
		g2d.fill(ball3);
		g2d.setColor(Color.BLACK);
		g2d.fill(ball4);
		g2d.setColor(Color.WHITE);
		g2d.fill(ball5);
	}	

}
