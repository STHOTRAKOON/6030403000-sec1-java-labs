package surakhan.thotrakoon.lab5;

import java.time.format.DateTimeFormatter;

/**
 * Boxer is a subclass of Athlete and has additional variables as sport that is
 * set to "Boxer", division, and golveSize.
 * 
 * 
 * @since 2018-02-05
 * @version 1.0
 * @author Thotrakoon Surakhan
 */

public class Boxer extends Athlete implements Playable, Movable{

	private String division;
	private String golveSize;
	private static String Sport = "Boxing";

	public Boxer(String name, double weight, double height, Gender gender, String nationality, String birthdate,
			String division, String golveSize) {
		super(name, weight, height, gender, nationality, birthdate);
		this.division = division;
		this.golveSize = golveSize;
	}

	public static String getSport() {
		return Sport;
	}

	public static void setSport(String sport) {
		Boxer.Sport = sport;
	}
	
	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getGolveSize() {
		return golveSize;
	}

	public void setGolveSize(String golveSize) {
		this.golveSize = golveSize;
	}

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	public String toString() {
		return "" + getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender() + ", "
				+ getNationality() + ", " + getBirthdate() + ", " + getSport() + ", " + getDivision() + ", "
				+ getGolveSize();
	}
	
	@Override
	public void play() {
		System.out.println(getName() + " throws a punch.");
	}
	@Override
	public void move() {
		System.out.println(getName() + " move around a boxing ring.");
	}
}
