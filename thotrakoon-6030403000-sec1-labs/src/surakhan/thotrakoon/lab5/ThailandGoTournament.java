package surakhan.thotrakoon.lab5;

public class ThailandGoTournament extends Competition {

	private String winner;
	
	public ThailandGoTournament(String name, String date, String place, String description) {
		super(name, date, place, description);
	}
	
	public void setDescriptionAndRules() {
		System.out.println("==== Begin: Description and Rules ================\n"
				+ "The competition is open for all students and people\n"
				+ "The competition is divided into four categories\n"
				+ "1. High ban (3 Dan and above)\n"
				+ "2.Low dan (1-2 Dan)\n"
				+ "3. High Kyu (1-4 Kyu)\n"
				+ "4. Low Kyu (5-8 Kyu)\n"
				+ "==== End: Description and Rules ==================");
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}
	
	@Override
	public String toString() {
		return getName() + " was held on " + getDate() + " in "
				+ getPlace() + ". \nSome winners are " + getWinner();
	}
}
