package surakhan.thotrakoon.lab5;

import java.time.format.DateTimeFormatter;

/**Footballer is a subclass of Athlete and has additional variables as sport that is set to "American Football", position, and team.
* 
* @since 2018-02-05
* @version 1.0
* @author Thotrakoon Surakhan
*/



public class Footballer extends Athlete implements Playable, Movable{

	private String position;
	private String team;
	private static String Sport = "American football";
	
	public Footballer(String name, double weight, double height, Gender gender, String nationality, String birthdate,
			String position, String team) {
		super(name, weight, height, gender,nationality,birthdate);
		this.position = position;
		this.team = team;
	}
	
	public static  String getSport() {
		return Sport;
	}
	
	public static void setSport(String sport) {
		Footballer.Sport = sport;
	}
	
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	public String toString() {
		return "" + getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender() + ", " + getNationality() + ", "
				+ getBirthdate() + ", " + getSport() + ", " + getPosition() + ", " + getTeam();
	}
	@Override
	public void play() {
		System.out.println(getName() + " throws a touchdown.");
	}
	@Override
	public void move() {
		System.out.println(getName() + " move down the football field.");
	}
}
