package surakhan.thotrakoon.lab5;

public class InterestingCompetitions {
	public static void main(String[] args) {
		SuperBowl superBowlLII = 
				new SuperBowl("SuperBowlLII", "04/02/2018", "Minnesota, United States",
						"the 52nd Super Bowl",
						" New England Patriots",
						"Philadelphia Eagles", "Philadelphia Eagles"); 
		superBowlLII.setDescriptionAndRules();
		System.out.println(superBowlLII);
		SuperBowl superBowlLI = 
				new SuperBowl("SuperBowlLII", "05/02/2017", "Texas, United States",
						"the 51sd Super Bowl",
						" New England Patriots",
						"Philadelphia Eagles", "New England Patriots"); 
		System.out.println(superBowlLI);
		
		ThailandGoTournament the20thGoOpen =
				new ThailandGoTournament("THE 20th THAILAND OPEN GO TOURNAMENT", 
						"20/08/2017","Samut Prakan, Thailand","");
		the20thGoOpen.setWinner("ประเภท High kyu รางวัลรองชนะเลิศอันดับ 2 ได้แก่ เมธัส แก้วทราย");
		the20thGoOpen.setDescriptionAndRules();
		System.out.println(the20thGoOpen);
	}
}
