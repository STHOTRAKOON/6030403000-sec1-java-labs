package surakhan.thotrakoon.lab5;

public class SuperBowl extends Competition {
	
	private String AFCTeam;
	private String NFCTeam;
	private String winningTeam;
	

		public SuperBowl(String name, String date, String place, String description) {
			super(name, date, place, description);
		}
		
		public SuperBowl(String name, String date, String place, String description,
				String AFCTeam, String NFCTeam, String winningTeam) {
			super(name, date, place, description);
			this.AFCTeam = AFCTeam;
			this.NFCTeam = NFCTeam;
			this.winningTeam = winningTeam;
		}

		public void setDescriptionAndRules() {
			System.out.println("==== Begin: Description and Rules ================\n"
					+ getName() + "is play between the champions of National Football Conference (NFC)\n"
					+ "and te American Football Conference (AFC).\n"
					+ "The game plays in four quarters while each quarter takes about 15 minutes.\n"
					+ "==== End: Description and Rules ==================");
		}

		public String getAFCTeam() {
			return AFCTeam;
		}

		public void setAFCTeam(String aFCTeam) {
			AFCTeam = aFCTeam;
		}

		public String getNFCTeam() {
			return NFCTeam;
		}

		public void setNFCTeam(String nFCTeam) {
			NFCTeam = nFCTeam;
		}

		public String getWinningTeam() {
			return winningTeam;
		}

		public void setWinningTeam(String winningTeam) {
			this.winningTeam = winningTeam;
		}
		
		@Override
		public String toString() {
			return getName() + "(" + getDescription() + ") " + "was held on "
					+ getDate() + " in " + getPlace() + ". \nIt was the game between "
					+ getAFCTeam() + " vs. " + getNFCTeam() + ".\nThe winner was "
					+ getWinningTeam() + ".";
		}
}
