package surakhan.thotrakoon.lab5;

/**Gender is an enum type which has values as either MALE or FEMALE
* 
* @since 2018-02-05
* @version 1.0
* @author Thotrakoon	Surakhan
*/

public enum Gender {
	MALE, FEMALE;
}
