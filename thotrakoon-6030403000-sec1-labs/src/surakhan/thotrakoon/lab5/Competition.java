package surakhan.thotrakoon.lab5;

public class Competition {

	public Competition(String name, String date, String place, String description) {
		super();
		this.name = name;
		this.date = date;
		this.place = place;
		this.description = description;
	}

	private String name;
	private String date;
	private String place;
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
