package surakhan.thotrakoon.lab5;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.format.DateTimeFormatter;

/**BadmintonPlayer is a subclass of Athlete and has additional variables as sport that is set to "Badminton", racketLength, and worldRanking. 
*
* @since 2018-02-05
* @version 1.0
* @author Thotrakoon Surakhan
*/
interface Playable {
	   public void  play();
	}
interface Movable {
	   public void  move();
	}

public class BadmintonPlayer extends Athlete implements Playable, Movable {
	
	private double racketLength;
	private int worldRanking;
	private static String Sport = "Badminton"; 
	protected String equipment = "shuttlecock";
	
	public BadmintonPlayer(String name, double weight, double height, Gender gender, String nationality,
			String birthdate, double racketLength, int worldRanking, String equipment) {
		super(name, weight, height, gender, nationality, birthdate);
		this.racketLength = racketLength;
		this.worldRanking = worldRanking;
		this.equipment = equipment;
	}
	
	public BadmintonPlayer(String name, double weight, double height, Gender gender, String nationality,
			String birthdate, double racketLength, int worldRanking) {
		super(name, weight, height, gender, nationality, birthdate);
		this.racketLength = racketLength;
		this.worldRanking = worldRanking;
	}
	
	public static  String getSport() {
		return Sport;
	} 
	
	public static void setSport(String sport) {
		BadmintonPlayer.Sport = sport;
	}
	
	public double getRacketLength() {
		return racketLength;
	}

	public void setRacketLength(double racketLength) {
		this.racketLength = racketLength;
	}

	public int getWorldRanking() {
		return worldRanking;
	}

	public void setWorldRanking(int worldRanking) {
		this.worldRanking = worldRanking;
	}
	
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender()
		+ ", " + getNationality() + ", " + getBirthdate() + getRacketLength() +"cm, " + "rank:" + getWorldRanking() + ", equipment: " + getEquipment();
	}
	public void compareAge(Athlete x) {
		LocalDate dateBefor = x.getBirthdate();
		LocalDate dateAfter = getBirthdate();
		long year = ChronoUnit.YEARS.between(dateBefor, dateAfter);
        
		if (year < 0){
            System.out.println ( x.getName ()+" is " + Math.abs(year) + " years older than " + getName () );
        }
        else {
            System.out.println ( getName ()+" is " + year + " years older than " + x.getName () );
        }
	}
	@Override
	public void play() {
		System.out.println(getName() + " hits a shuttlecock.");
	}
	@Override
	public void move() {
		System.out.println(getName() + " move around badminton court.");
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	
	
}
