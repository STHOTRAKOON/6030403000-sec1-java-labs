package surakhan.thotrakoon.lab5;

import java.time.format.DateTimeFormatter;
import java.time.LocalDate;


/**Athlete is a class to explain information of athletes (name, weight, height, gender, nationality, birthdate)
 * 
* @since 2018-02-05
* @version 1.0
* @author Thotrakoon	Surakhan
*/

public class Athlete {

	public Athlete(String name, double weight, double height, Gender gender, String nationality, String birthdate) {
		super();
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;
		this.birthdate = LocalDate.parse(birthdate, formatter);
	}

	private String name;
	private double weight;
	private double height;
	private Gender gender;
	private String nationality;
	private LocalDate birthdate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Athlete [" + name + ", " + weight + ", " + height + ", " + gender + ", " + nationality + ", "
				+ birthdate + "]";
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy"); 
	
	public void playSport() {
		String nameSport = Boxer.getSport();
		System.out.println(name + "is good at " + nameSport);
	}
	
}
