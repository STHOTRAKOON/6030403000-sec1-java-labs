package surakhan.thotrakoon.lab6;

public class TestAuthor {
	public static void main(String[] args) {
		Author win = new Author("วินทร์ เลียววาริณ", "win@winbookclub.com", 'm'); // Test the constructor
		System.out.println(win);  // Test toString()
		win.setEmail("win@gmail.com");  // Test setter
		System.out.println("name is: " + win.getName());     // Test getter
		System.out.println("email is: " + win.getEmail());   // Test getter
		System.out.println("gender is: " + win.getGender()); // Test gExerciseOOP_MyPolynomial.pngetter
	}
}

