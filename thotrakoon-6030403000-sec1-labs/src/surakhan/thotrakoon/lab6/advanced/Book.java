package surakhan.thotrakoon.lab6.advanced;

import surakhan.thotrakoon.lab6.Author;

public class Book {

	public Book(String name, Author[] authors, double price, int qty) {
		super();
		this.name = name;
		this.price = price;
		this.qty = qty;
		this.authors = authors;
	}

	public Book(String name, Author[] authors, double price) {
		super();
		this.name = name;
		this.price = price;
		this.authors = authors;
	}

	private String name;
	private double price;
	private int qty = 0;
	private Author[] authors;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public Author[] getAuthors() {
		return authors;
	}

	public void setAuthors(Author[] authors) {
		this.authors = authors;
	}

	@Override
	public String toString() {
		String stringOfauthor = "";
		for (int i = 0; i < authors.length; i++) {//loop เพื่อเอาข้อมูล คนเขียน รวมไว้ใน stringOfauthor
			stringOfauthor = stringOfauthor + authors[i];
			if (i != authors.length - 1) {
				stringOfauthor = stringOfauthor + ",\n";
			}
		}
		return "Book[name = " + name + ", \nauthors={" + stringOfauthor + "}, price = " + price + ", qty = " + qty
				+ "]";
	}

	public String getAuthorNames() {
		String stringOfName = "";
		for (int i = 0; i < authors.length; i++) {//loop เพื่อเอาชื่อ คนเขียน รวมไว้ใน stringOfName
			stringOfName = stringOfName + authors[i].getName();
			if (i != authors.length - 1) {
				stringOfName = stringOfName + ", ";
			}
		}
		return stringOfName;
	}
}
