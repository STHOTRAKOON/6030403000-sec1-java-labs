package surakhan.thotrakoon.lab6;

import surakhan.thotrakoon.lab5.BadmintonPlayer;
import surakhan.thotrakoon.lab5.Gender;

public class ThaiBadmintonPlayer extends BadmintonPlayer {
	private String equipment = "ลูกขนไก่";

	public ThaiBadmintonPlayer(String name, double weight, double height, Gender gender, String birthdate,
			double racketLength, int worldRanking) {
		super(name, weight, height, gender, "Thai", birthdate, racketLength, worldRanking);

	}

	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender() + ", " + getNationality()
				+ ", " + getBirthdate() + getRacketLength() + "cm, " + "rank:" + getWorldRanking() + ", equipment: "
				+ equipment;
	}

	public void play() {
		super.play();
		System.out.println(getName() + " hits " + equipment);
	}

}
