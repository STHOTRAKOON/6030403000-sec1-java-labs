package surakhan.thotrakoon.lab2;

/*
 *  Enter <name> <ID> <GPA> <academic year> <parents's income>
 * then show your data and how different from the first CoE student's ID
 */
public class CoEKKUStudent {
	public static void main(String[] args) {
		if (args.length == 5) {
			String name = args[0];
			long id = Long.valueOf(args[1]).longValue(); // ID
			float gpa = Float.valueOf(args[2]).floatValue();// GPA
			short year = Short.valueOf(args[3]).shortValue();// the entered academic year
			float income = Float.valueOf(args[4]).floatValue();// estimated parentsís income
			long diff = id - 3415717;// different from the first CoE student
			System.out.println("" + name + " has ID = " + id + " GPA = " + gpa + " year = " + year
					+ "parent's income = " + income);
			System.out.println("Your ID different from the first CoE student's ID by " + diff);
		} else {
			System.err.println("CoEKKUStudent <name> <ID> <GPA> <academic year> <parents's income>");
		}
	}
}
