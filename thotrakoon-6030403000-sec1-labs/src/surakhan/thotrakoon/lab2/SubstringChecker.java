package surakhan.thotrakoon.lab2;

/* 
 * accepts at least two input arguments and up to as many arguments as possible. 
 * The first one is the substring to check whether it is contained in the other input arguments.
 * Then the program displays the output to specify the number of strings 
 * that contain the first substring and the number of the string that contains that substring.
 */
public class SubstringChecker {
	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("SubstringChecker <Subtring> <str1> ...");
		} else {
			String sub = args[0]; // substring
			int number = 0; // จำนวนที่สตริงที่มี sub บรรจุอยู่
			for (int x = 1; x < args.length; x++) { // x, y, z เป็นตัวแปรที่รันในลูปเท่านั้น
				String str = args[x];
				for (int y = 0; y < str.length() + 1 - sub.length(); y++) {
					int a = 0;
					for (int z = 0; z < sub.length(); z++) {
						if (sub.charAt(z) == str.charAt(y + a)) {
							a++;
							if (a == sub.length()) {
								number++;
								System.out.println("String " + x + ": " + str + "  that contains " + sub);
							}
						} else
							break;
					}
				}
			}
			if (number == 0) {
				System.out.println("there are no strings that contain " + sub);
			} else if (number == 1) {
				System.out.println("There is " + number + " string that contain " + sub);
			} else if (number >= 2) {
				System.out.println("There are " + number + "strings that contain " + sub);
			}
		}
	}
}
