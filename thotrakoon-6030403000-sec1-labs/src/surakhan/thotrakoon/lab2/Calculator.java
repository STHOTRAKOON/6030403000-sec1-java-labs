package surakhan.thotrakoon.lab2;

//for calculation only " + " " - " " * " " / "
import java.text.DecimalFormat;

public class Calculator {
	public static void main (String[] args) {
		if (args.length <= 2) {
			System.err.println("Calculator <operand1> <operand2> <operator>");
		}
		else {
			float operand1 = Float.valueOf(args[0]).floatValue(); // เปลี่ยน String(args[0]) เป็น  Float(operand1)
			float operand2 = Float.valueOf(args[1]).floatValue(); // เปลี่ยน String(args[1]) เป็น  Float(operand2)
			String operator = args[2]; 
			if (operator.equals("/")) {
				if (operand2 == 0) {
					System.out.println("The second operant cannot be zero");
				}
				else {
					float answer = operand1 / operand2 ;
					String pattern = "0.00"; 
					DecimalFormat df = new DecimalFormat(pattern); 
					String output = df.format(answer);
					System.out.println("" + operand1 + " " + operator + " " + operand2 + " = " + output);
				}
			}
			else if (operator.equals("*") || operator.equals(".classpath")) {
				float answer = operand1 * operand2 ;
				operator = "*";
				System.out.println("" + operand1 + " " + operator + " " + operand2 + " = " +answer);
			}
			else if (operator.equals("+")) {
				float answer = operand1 + operand2 ; 
				System.out.println("" + operand1 + " " + operator + " "  + operand2 + " = " +answer);
			}
			else if (operator.equals("-")) {
				float answer = operand1 - operand2 ; 
				System.out.println("" + operand1 +  " " + operator + " " + operand2 + " = " +answer);
			}
		}
		
	}
}
