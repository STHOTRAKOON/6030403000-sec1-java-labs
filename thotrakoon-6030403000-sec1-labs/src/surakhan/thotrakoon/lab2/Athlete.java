package surakhan.thotrakoon.lab2;

/* It accepts three arguments: athlete name, the sport name that that athlete plays, and the nationality of that athlete.
Its output format is �My favorite athlete is <athlete_name> who plays <sport_name> and has nationality as <athlete_nationality>�
*/
public class Athlete {
	public static void main(String[] args) {

		if (args.length == 3) {
			String athlete_name = args[0];// athlete name
			String sport_name = args[1];// sport name
			String athlete_nationality = args[2];// athlete nationality
			System.out.println("My favorite athlete is " + athlete_name + " who plays " + sport_name
					+ " and has nationality as " + athlete_nationality);
		} else {
			System.err.println("Athlete <athlete name>  <sport name> <athlete nationality>");
		}
	}
}
