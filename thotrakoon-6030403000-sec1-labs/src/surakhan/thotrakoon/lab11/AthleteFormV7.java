package surakhan.thotrakoon.lab11;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import surakhan.thotrakoon.lab9.AthleteFormV6;

public class AthleteFormV7 extends AthleteFormV6 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JFileChooser filechooser = new JFileChooser();
	protected int returnVal;
	
	public AthleteFormV7(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	public void setMAKeys(JMenuItem menu, 
			int mKey, int aKey, ActionListener listener) {
		menu.setMnemonic(mKey);
		menu.setAccelerator(KeyStroke.getKeyStroke(
				aKey, ActionEvent.CTRL_MASK));
		menu.addActionListener(listener);
	}
	public void enableKeyboard() {
		fileMenu.setMnemonic(KeyEvent.VK_F);
		fileMenu.addActionListener(this);
		setMAKeys(newMI, KeyEvent.VK_N, KeyEvent.VK_N, this);
		setMAKeys(openMI, KeyEvent.VK_O, KeyEvent.VK_O, this);
		setMAKeys(saveMI, KeyEvent.VK_S, KeyEvent.VK_S, this);
		setMAKeys(exitMI, KeyEvent.VK_Q, KeyEvent.VK_Q, this);
	}
	
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		super.actionPerformed(event);
		if(src == openMI) {
			returnVal = filechooser.showOpenDialog(AthleteFormV7.this);
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				File file = filechooser.getSelectedFile();
				JOptionPane.showMessageDialog(this, "Opening:" + file + ".");
			}else {
				JOptionPane.showMessageDialog(this, "Opening command cancelled by user.");
			}
		}else if(src == saveMI) {
			returnVal = filechooser.showSaveDialog(AthleteFormV7.this);
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				File file = filechooser.getSelectedFile();
				JOptionPane.showMessageDialog(this, "Saving:" + file + ".");
			}else {
				JOptionPane.showMessageDialog(this, "Saving command cancelled by user.");
			}
		}else if(src == exitMI) {
			System.exit(0);
		}else if(src == colorCustom) {
			Color color = JColorChooser.showDialog(null, "Choose Color", Color.WHITE);
			compTxtArea.setBackground(color);
		}
	}
	
	protected void addListeners() {
		super.addListeners();
		colorCustom.addActionListener(this);
	}
	
	public static void createAndShowGUI() {
		AthleteFormV7 athleteFormV7 = new AthleteFormV7("Athlete Form V7");
		athleteFormV7.addMenus();
		athleteFormV7.enableKeyboard();
		athleteFormV7.addComponents();
		athleteFormV7.setFrameFeatures();
		athleteFormV7.addListeners();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
