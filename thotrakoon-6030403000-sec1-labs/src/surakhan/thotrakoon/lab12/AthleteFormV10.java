package surakhan.thotrakoon.lab12;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class AthleteFormV10 extends AthleteFormV9 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected FileWriter FileWriter;
	protected String line, s;
	protected BufferedReader BufferedReader;

	public AthleteFormV10(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public void handleOpenMI() {
		int returnVa = returnVal;
		if (returnVa == JFileChooser.APPROVE_OPTION) {
			try {
				File file = filechooser.getSelectedFile();

				BufferedReader = new BufferedReader(new FileReader(file.getPath()));
				line = "";
				s = "";
				while ((line = BufferedReader.readLine()) != null) {
					s += line + "\n";
				}
				JOptionPane.showMessageDialog(this, s);
				if (BufferedReader != null) {
					BufferedReader.close();
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, "Somrthing happen! please try again.", "Warning",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	public void handleSaveMI() {
		int returnVa = returnVal;
		if (returnVa == JFileChooser.APPROVE_OPTION) {
			File file = filechooser.getSelectedFile();
			try {

				
				FileWriter = new FileWriter(file.getPath());
				String display = "";
				for (int i = 0; i < ArrAthlete.size(); i++) {
					display += (i + 1) + ". " + ArrAthlete.get(i) + "\n";
				}
				FileWriter.write(display);
				FileWriter.flush(); 
							
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, "Somrthing happen! please try again.", "Warning",
						JOptionPane.ERROR_MESSAGE);
			}
			JOptionPane.showMessageDialog(this, "Saving : " + file.getName() + "");
		}
	}

	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		super.actionPerformed(event);
		if (src == openMI) {
			handleOpenMI();
		} else if (src == saveMI) {
			handleSaveMI();
		}

	}

	protected void addListeners() {
		super.addListeners();
		openMI.addActionListener(this);
		openMI.removeActionListener(openMI.getActionListeners()[0]);
		saveMI.addActionListener(this);
		saveMI.removeActionListener(saveMI.getActionListeners()[0]);
	}

	public static void createAndShowGUI() {
		AthleteFormV10 athleteFormV10 = new AthleteFormV10("Athlete Form V10");
		athleteFormV10.addMenus();
		athleteFormV10.enableKeyboard();
		athleteFormV10.addComponents();
		athleteFormV10.setFrameFeatures();
		athleteFormV10.addListeners();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
