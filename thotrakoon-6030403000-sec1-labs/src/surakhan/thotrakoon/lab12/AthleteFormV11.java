package surakhan.thotrakoon.lab12;

import java.time.format.DateTimeParseException;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class AthleteFormV11 extends AthleteFormV10 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public double weightDouble, heightDouble;

	public AthleteFormV11(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public void objectAthlete() {
		// weight
		try {

			if (weightTxtField.getText().length() == 0 && !weightTxtField.getText().matches("[0-9]+")) {
				throw new NumberFormatException("Please enter weight as double value");
			} else {
				weightDouble = Double.parseDouble(weightTxtField.getText());
			}
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}

		// height
		try {
			if (heightTxtField.getText().length() == 0 && !heightTxtField.getText().matches("[0-9]+")) {
				throw new NumberFormatException("Please enter height as double value");
			} else {
				heightDouble = Double.parseDouble(heightTxtField.getText());
			}
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}

		// birthdate
		try {
			if (dateTxtField.getText().length() == 0 || !(dateTxtField.getText().matches("\\d{2}/\\d{2}/\\d{4}"))) {
				throw new DateTimeParseException("Please enter date in the format dd/MM/yyyy  such as 02/02/2000",
						dateTxtField.getText(), 0);
			}
		} catch (DateTimeParseException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}

		// name
		try {

			if (!nameTxtField.getText().matches("^\\d+(\\.\\d+)?")) {
				throw new NoNameException("Please enter athlete name");
			}
		} catch (NoNameException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
	}

	public static void createAndShowGUI() {
		AthleteFormV11 athleteFormV11 = new AthleteFormV11("Athlete Form V11");
		athleteFormV11.addMenus();
		athleteFormV11.enableKeyboard();
		athleteFormV11.addComponents();
		athleteFormV11.setFrameFeatures();
		athleteFormV11.addListeners();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	class NoNameException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public NoNameException() {
			super();
		}

		public NoNameException(String msg) {
			super(msg);
		}

	}

}
