package surakhan.thotrakoon.lab9;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

public class AthleteFormV5 extends AthleteFormV4 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JMenu color,size;
	Object src;
	protected JMenuItem blue, green, red, colorCustom;
	protected JMenuItem size16, size20, size24, sizeCustom;

	public AthleteFormV5(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	protected void addMenus() {
		initMenus();
		fileMenu.add(newMI);
		fileMenu.add(openMI);
		fileMenu.add(saveMI);
		fileMenu.add(exitMI);
		color = new JMenu("Color");
		blue = new JMenuItem("Blue");
		green = new JMenuItem("Green");
		red = new JMenuItem("Red");
		colorCustom = new JMenuItem("Custom...");
		color.add(blue);
		color.add(green);
		color.add(red);
		color.add(colorCustom);
		configMenu.add(color);
		size = new JMenu("Size");
		size16 = new JMenuItem("16");
		size20 = new JMenuItem("20");
		size24 = new JMenuItem("24");
		sizeCustom = new JMenuItem("Custom...");
		size.add(size16);
		size.add(size20);
		size.add(size24);
		size.add(sizeCustom);
		configMenu.add(size);
		menuBar.add(fileMenu);
		menuBar.add(configMenu);
		setJMenuBar(menuBar);
	}

	protected void addListeners() {
		super.addListeners();
		blue.addActionListener(this);
		green.addActionListener(this);
		red.addActionListener(this);
		size16.addActionListener(this);
		size20.addActionListener(this);
		size24.addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		src = event.getSource();
		if(src == blue) {
			colorHandle(Color.BLUE);
		}else if(src == green) {
			colorHandle(Color.GREEN);
		}else if(src == red) {
			colorHandle(Color.RED);
		}else if(src ==size16) {
			Font font = new Font("SansSerif", Font.BOLD, 16);
			sizeHandle(font);
		}else if(src ==size20) {
			Font font = new Font("SansSerif", Font.BOLD, 20);
			sizeHandle(font);
		}else if(src ==size24) {
			Font font = new Font("SansSerif", Font.BOLD, 24);
			sizeHandle(font);
		}
	}
	
	protected void colorHandle(Color color2) {
		compTxtArea.setForeground(color2);
		nameTxtField.setForeground(color2);
		dateTxtField.setForeground(color2);
		weightTxtField.setForeground(color2);
		heightTxtField.setForeground(color2);
		nationalityTxtField.setForeground(color2);
	}
	
	protected void sizeHandle(Font font2) {
		compTxtArea.setFont(font2);
		nameTxtField.setFont(font2);
		dateTxtField.setFont(font2);
		weightTxtField.setFont(font2);
		heightTxtField.setFont(font2);
		nationalityTxtField.setFont(font2);
	}

	public static void createAndShowGUI() {
		AthleteFormV5 athleteFormV5 = new AthleteFormV5("Athlete Form V5");
		athleteFormV5.addMenus();
		athleteFormV5.addComponents();
		athleteFormV5.setFrameFeatures();
		athleteFormV5.addListeners();
		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
