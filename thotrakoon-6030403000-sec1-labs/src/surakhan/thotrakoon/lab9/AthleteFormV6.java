package surakhan.thotrakoon.lab9;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AthleteFormV6 extends AthleteFormV5 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JPanel panel1, panel2;

	public AthleteFormV6(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public void addComponents() {
		super.addComponents();
		panel1 = new JPanel(new FlowLayout());
		panel2 = new JPanel(new BorderLayout());
		ImageIcon pic = new ImageIcon("images/athlete.jpg");
		Image image = pic.getImage();
		Image newimg = image.getScaledInstance(150, 120, java.awt.Image.SCALE_SMOOTH);
		ImageIcon imageIcon = new ImageIcon(newimg);
		int x = imageIcon.getIconHeight();
		int y = imageIcon.getIconWidth();
		panel1.add(new JLabel(imageIcon));
		panel1.setPreferredSize(new Dimension(x, y));
		panel2.add(panel1, BorderLayout.NORTH);
		panel2.add(overallPanel, BorderLayout.SOUTH);
		setContentPane(panel2);

	}

	protected void addMenus() {
		super.addMenus();
		ImageIcon open = new ImageIcon("images/open.png");
		ImageIcon save = new ImageIcon("images/save.png");
		ImageIcon exit = new ImageIcon("images/exit.png");
		Image image = open.getImage();
		Image newimg = image.getScaledInstance(10, 10, java.awt.Image.SCALE_SMOOTH);
		ImageIcon openIcon = new ImageIcon(newimg);
		Image image1 = save.getImage();
		Image newimg1 = image1.getScaledInstance(10, 10, java.awt.Image.SCALE_SMOOTH);
		ImageIcon saveIcon = new ImageIcon(newimg1);
		Image image2 = exit.getImage();
		Image newimg2 = image2.getScaledInstance(10, 10, java.awt.Image.SCALE_SMOOTH);
		ImageIcon exitIcon = new ImageIcon(newimg2);
		openMI.setIcon(openIcon);
		saveMI.setIcon(saveIcon);
		exitMI.setIcon(exitIcon);
	}

	public static void createAndShowGUI() {
		AthleteFormV6 athleteFormV6 = new AthleteFormV6("Athlete Form V6");
		athleteFormV6.addMenus();
		athleteFormV6.addComponents();
		athleteFormV6.setFrameFeatures();
		athleteFormV6.addListeners();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
