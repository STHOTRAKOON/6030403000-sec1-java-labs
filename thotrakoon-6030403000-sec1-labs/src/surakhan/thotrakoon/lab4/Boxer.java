package surakhan.thotrakoon.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Boxer {
public Boxer(String name, double weight, double height, Gender gender, String nationality, String birthdate,
			String division, String golveSize) {
		super();
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;
		this.birthdate = LocalDate.parse(birthdate, formatter);
		this.division = division;
		this.golveSize = golveSize;
	}

	private String name;
	private double weight;
	private double height;
	private Gender gender;
	private String nationality;
	private LocalDate birthdate;
	private String division;
	private String golveSize;
	private static String Sport = "Boxing";
	
	public static  String getSport() {
		return Sport;
	} 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public LocalDate getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getGolveSize() {
		return golveSize;
	}
	public void setGolveSize(String golveSize) {
		this.golveSize = golveSize;
	}
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy"); //กำหนดรูปแบบของวันเกิด
	public String toString() {
		return "" + name + ", " + weight + "kg, " + height + "m, " + gender + ", " + nationality + ", "
				+ birthdate + ", " + Sport + ", " + division + ", " + golveSize;
}
}
