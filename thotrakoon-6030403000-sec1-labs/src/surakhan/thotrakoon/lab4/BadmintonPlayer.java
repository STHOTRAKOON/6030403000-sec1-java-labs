package surakhan.thotrakoon.lab4;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.format.DateTimeFormatter;

public class BadmintonPlayer {
	public BadmintonPlayer(String name, double weight, double height, Gender gender, String nationality,
			String birthdate, double racketLength, int worldRanking) {
		super();
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;
		this.birthdate = LocalDate.parse(birthdate, formatter);
		this.racketLength = racketLength;
		this.worldRanking = worldRanking;
	}

	private String name;
	private double weight;
	private double height;
	private Gender gender;
	private String nationality;
	private LocalDate birthdate;
	private double racketLength;
	private int worldRanking;
	private static String Sport = "Badminton"; 
	
	public void compareAge(Footballer Athlete) {//ฟังก์ชันที่คำนวณระหว่าง BadmintonPlayer กับ Footballer
		LocalDate dateBefore = birthdate;
		LocalDate dateAfter = Athlete.getBirthdate();
		int year = (int) ChronoUnit.YEARS.between(dateBefore, dateAfter);
		if(year > 0) {
			System.out.println(name +  " is " + year + " years older than " + Athlete.getName());
		}
		else {
			System.out.println(Athlete.getName() +  " is " + year * -1 + " years older than " + name);
		}
	}
	
	public void compareAge(BadmintonPlayer Athlete) {//ฟังก์ชันที่คำนวณระหว่าง BadmintonPlayer กับ BadmintonPlayer
		LocalDate dateBefore = birthdate;
		LocalDate dateAfter = Athlete.getBirthdate();
		int year = (int) ChronoUnit.YEARS.between(dateBefore, dateAfter);
		if(year > 0) {
			System.out.println(name +  " is " + year + " years older than " + Athlete.getName());
		}
		else {
			System.out.println(Athlete.getName() +  " is " + year * -1 + " years older than " + name);
		}
	}
	
	public static  String getSport() {
		return Sport;
	} 
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public double getRacketLength() {
		return racketLength;
	}

	public void setRacketLength(double racketLength) {
		this.racketLength = racketLength;
	}

	public int getWorldRanking() {
		return worldRanking;
	}

	public void setWorldRanking(int worldRanking) {
		this.worldRanking = worldRanking;
	}

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");//กำหนดรูปแบบของวันเกิด
	public String toString() {
		return "" + name + ", " + weight + "kg, " + height + "m, " + gender + ", " + nationality + ", "
				+ birthdate + ", " + Sport + ", " + racketLength + "cm, rank:" + worldRanking;
	}
	
}
