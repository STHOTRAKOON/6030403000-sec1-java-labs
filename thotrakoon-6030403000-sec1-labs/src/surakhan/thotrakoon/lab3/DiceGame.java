package surakhan.thotrakoon.lab3;

/* This is a program that simulates a game of two players where one player (a human player) guests 
 * a score that another player (a computer player) will get from roll a dice.
 * The computer player rolls a dice (for number between 1 to 6) for a score. 
 * If the guest number is the same or close to the score then the human player wins, 
 * if the guest is not the same nor close to the score then the computer player wins.
*/
import java.util.Scanner;
import java.util.Random;

public class DiceGame {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter your guest (1-6) : ");
		int guest = scanner.nextInt();
		scanner.close();
		Random random = new Random();
		int answer = random.nextInt(6) + 1;
		if (guest > 0 && guest < 7) {
			System.out.println("You guested nember : " + guest);
			System.out.println("Computer has rolled number : " + answer);
			if (Math.abs(guest - answer) <= 1 || Math.abs(guest - answer) == 5) {
				System.out.println("You win.");
			} else {
				System.out.println("Computer wins.");
			}
		} else {
			System.err.println("Incorrect number. Only 1-6 can be entered");
		}
	}
}