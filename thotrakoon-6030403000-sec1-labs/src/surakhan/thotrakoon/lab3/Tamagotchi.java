package surakhan.thotrakoon.lab3;

/*This is a program that simulates a digital pet where you have to take care of the pet. 
 * You are to write a program that read in three integers from user via console windows using Java Scanner class. 
 * The first integer represents how many hours your tamagotchi pet will live. 
 * The second number is a time interval when you have to feed your pet. 
 * And the last integer is a time interval when you have to water your pet.
 */
import java.util.Scanner;

public class Tamagotchi {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter your Tamagotchi info :");
		int live = scanner.nextInt();
		int fed = scanner.nextInt();
		int watered = scanner.nextInt();
		scanner.close();
		System.out.println("Your Tamagotchi will live for " + live + " hours");
		System.out.println("It needs to be fed every " + fed + " hours");
		System.out.println("It needs to be watered every " + watered + " hours");
		System.out.println(" ");
		int waterandfeed = 0; //Number of times to feed and water in the same time
		int water = 0; // Number of times to water
		int feed = 0; // Number of times to feed
		for (int i = 1; i <= live; i++) {
			if (i % watered == 0) {
				water++;
			}
			if (i % fed == 0) {
				feed++;
			}
			if (i % watered == 0 && i % fed == 0) {
				waterandfeed++;
			}
		}
		water = water - waterandfeed;
		feed = feed - waterandfeed;
		System.out.println("You need to water-feed : " + waterandfeed + " times");
		System.out.println("You need to water : " + water + " times");
		System.out.println("You need to feed : " + feed + " times");

	}
}

