package surakhan.thotrakoon.lab7;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.*;


public class MySimpleWindow extends JFrame {


	private static final long serialVersionUID = 1L;
	protected JButton cancelButton , okButton ;
	protected JPanel selectPanel ;

	public MySimpleWindow(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	private void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	protected Component addComponents() {
		
		cancelButton = new JButton("Cancel");
		okButton = new JButton("  OK  ");
		selectPanel = new JPanel();
		selectPanel.add(cancelButton);
		selectPanel.add(okButton);
		this.setContentPane(selectPanel);
		return selectPanel;
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
