package surakhan.thotrakoon.lab7;

import javax.swing.*;
import java.awt.*;
public class AthleteFormV3 extends AthleteFormV2{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected JLabel type;

	public AthleteFormV3(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
    public static void createAndShowGUI(){
	AthleteFormV3 AthleteForm3 = new AthleteFormV3("Athlete Form V3");
	AthleteForm3.addMenu();
	AthleteForm3.addComponents();
	AthleteForm3.setFrameFeatures();
    }
    
    private void setFrameFeatures() {
    	pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    protected Component addComponents() {
    	JPanel typePanel = new JPanel();
    	typePanel.setLayout(new GridLayout(1,2));
    	JPanel typeLabel = new JPanel();
    	type = new JLabel("Type:");
    	typeLabel.add(type);
    	typePanel.add(typeLabel);
    	
    	JPanel sportType = new JPanel();
    	String[] sportstring = {"Badminton Player", "Boxer", "Footballer"}; 
    	JComboBox sport = new JComboBox(sportstring);
    	sport.setSelectedItem("Boxer");
    	sport.setEditable(true);
    	sportType.add(sport);
    	typePanel.add(sportType);
    	
    	JPanel AthleteFormV3Panel = new JPanel();
    	AthleteFormV3Panel.setLayout(new BorderLayout());
    	AthleteFormV3Panel.add(super.addComponents(), BorderLayout.NORTH);
    	AthleteFormV3Panel.add(typePanel, BorderLayout.CENTER);
    	AthleteFormV3Panel.add(selectPanel, BorderLayout.SOUTH);
    	this.setContentPane(AthleteFormV3Panel);
    	return AthleteFormV3Panel;
    	
    }
    
    public void addMenu() {
    	JMenuBar menuBar = new JMenuBar();
    	JMenu file = new JMenu("file");
    	JMenuItem newMI = new JMenuItem("New");
    	JMenuItem openMI = new JMenuItem("Open");
    	JMenuItem saveMI = new JMenuItem("Save");
    	JMenuItem exitMI = new JMenuItem("Exit");
    	file.add(newMI);
    	file.add(openMI);
    	file.add(saveMI);
    	file.add(exitMI);
    	menuBar.add(file);
    	
    	JMenu config = new JMenu("config");
    	JMenuItem colorMI = new JMenuItem("Color");
    	JMenuItem sizeMI = new JMenuItem("Size");
    	config.add(colorMI);
    	config.add(sizeMI);
    	menuBar.add(config);
    	this.setJMenuBar(menuBar);
    }

public static void main(String[] args) {
	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			createAndShowGUI();
		}
	});
}

}
