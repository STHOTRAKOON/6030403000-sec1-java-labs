package surakhan.thotrakoon.lab7;

import javax.swing.*;
import java.awt.*;

public class AthleteFormV2 extends AthleteFormV1 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JLabel gender, compertition, text;
	protected JTextField genderJTextField, compertitionJTextField;
	protected JPanel genderANDcompertitionPanel, 
	genderANDcompertitionLabel;

	public AthleteFormV2(String title) {
		super(title);
	}
    public static void createAndShowGUI(){
	AthleteFormV2 AthleteForm2 = new AthleteFormV2("Athlete Form V2");
	AthleteForm2.addComponents();
	AthleteForm2.setFrameFeatures();
    }
    
    private void setFrameFeatures() {
    	pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    protected Component addComponents() {
    	JPanel genderANDcompertitionLabel = new JPanel();
    	genderANDcompertitionLabel.setLayout(new GridLayout(2,1));
    	gender = new JLabel("Gender:");
    	compertition = new JLabel("Compertition:");
    	genderANDcompertitionLabel.add(gender);
    	genderANDcompertitionLabel.add(compertition);
    	
    	JPanel genderJTextFieldPanel = new JPanel();
    	genderJTextFieldPanel.setLayout(new GridLayout(2,1));
    	ButtonGroup choices = new ButtonGroup();
    	JRadioButton male = new JRadioButton("Male", true);
    	JRadioButton female = new JRadioButton("Female");
    	choices.add(male);
    	choices.add(female);
    	JPanel group = new JPanel();
    	group.add(male);
    	group.add(female);
    	genderJTextFieldPanel.add(group);
    	
    	JTextArea compertitionTextPanel = new JTextArea(2, 35);
    	compertitionTextPanel.setText("Competed in the 31st national championship\n");
    	compertitionTextPanel.append("");
    	compertitionTextPanel.setLineWrap(true);
    	JScrollPane sp = new JScrollPane(compertitionTextPanel);
    	
    	JPanel genderLabelAndgenderText = new JPanel();
    	genderLabelAndgenderText.setLayout(new BorderLayout());
    	genderLabelAndgenderText.add(genderANDcompertitionLabel, BorderLayout.WEST); 
    	genderLabelAndgenderText.add(genderJTextFieldPanel, BorderLayout.EAST);
    	genderLabelAndgenderText.add(sp, BorderLayout.SOUTH);
    	
    	JPanel allPanel = new JPanel();
    	allPanel.setLayout(new BorderLayout());
    	allPanel.add(super.addComponents(),BorderLayout.NORTH);
    	allPanel.add(genderLabelAndgenderText,BorderLayout.CENTER);
    	allPanel.add(selectPanel,BorderLayout.SOUTH);
    	this.setContentPane(allPanel);
    	return allPanel;
    }

public static void main(String[] args) {
	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			createAndShowGUI();
		}
	});
}


}
