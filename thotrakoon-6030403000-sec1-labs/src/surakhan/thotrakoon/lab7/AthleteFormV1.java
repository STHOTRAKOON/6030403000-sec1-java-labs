package surakhan.thotrakoon.lab7;

import javax.swing.*;
import java.awt.*;

public class AthleteFormV1  extends MySimpleWindow {

	private static final long serialVersionUID = 1L;
	protected JLabel name, birthdate,weight, height, nationality;
	protected JTextField nameJTextField, birthdateJTextField, 
	weightJTextField, heightJTextField, nationalityJTextField;
	public int length = 15;
	
	
	public AthleteFormV1(String title) {
		super(title);
		
	}
    public static void createAndShowGUI(){
	AthleteFormV1 athleteForm1 = new AthleteFormV1("Athlete Form V1");
	athleteForm1.addComponents();
	athleteForm1.setFrameFeatures();
    }
    
    private void setFrameFeatures() {
    	pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    protected Component addComponents() {
    	JPanel cp = (JPanel) getContentPane();
		cp.setLayout(new BorderLayout());
    	JPanel JLabelPanel = new JPanel();
    	JLabelPanel.setLayout(new GridLayout(5,1));
    	name = new JLabel("Name:");
    	birthdate = new JLabel("Birthdate:");
    	weight = new JLabel("Weight(k.g):");
    	height = new JLabel("Height(metre):");
    	nationality = new JLabel("Nationality:");
    	JLabelPanel.add(name);
    	JLabelPanel.add(birthdate);
    	JLabelPanel.add(weight);
    	JLabelPanel.add(height);
    	JLabelPanel.add(nationality);
    	
    	JPanel JTextFieldPanel = new JPanel();
    	JTextFieldPanel.setLayout(new GridLayout(5,1));
    	nameJTextField = new JTextField(length);
    	birthdateJTextField = new JTextField(length); 
    	birthdateJTextField.setToolTipText("ex.22.02.2000");
    	weightJTextField = new JTextField(length); 
    	heightJTextField = new JTextField(length); 
    	nationalityJTextField = new JTextField(length); 
    	JTextFieldPanel.add(nameJTextField);
    	JTextFieldPanel.add(birthdateJTextField);
    	JTextFieldPanel.add(weightJTextField);
    	JTextFieldPanel.add(heightJTextField);
    	JTextFieldPanel.add(nationalityJTextField);
    	
    	JPanel JLabelandJTextField = new JPanel();
    	JLabelandJTextField.setLayout(new GridLayout(1,2));
    	JLabelandJTextField.add(JLabelPanel);
    	JLabelandJTextField.add(JTextFieldPanel);
    	cp.add(JLabelandJTextField, BorderLayout.CENTER);
		cp.add(super.addComponents(), BorderLayout.SOUTH);
		this.setContentPane(cp);
		return JLabelandJTextField;
    }
public static void main(String[] args) {
	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			createAndShowGUI();
		}
	});
}
}
