package surakhan.thotrakoon.lab10;

import javax.swing.SwingUtilities;


public class PongGameV1 extends SimpleGameWindow{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PongGameV1(String string) {
		super(string);
	}

	public static void createAndShowGUI() {
		PongGameV1 window = new PongGameV1("CoE Pong Game V1");
		window.setFrameFeatures();
		window.addComponents();
		
	}
	
	private void addComponents() {
		setContentPane(new PongGamePanel());
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
