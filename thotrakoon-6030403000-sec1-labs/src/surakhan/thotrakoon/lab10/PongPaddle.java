package surakhan.thotrakoon.lab10;

import java.awt.geom.Rectangle2D;

public class PongPaddle extends Rectangle2D.Double{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3420859037732960535L;
	public static int HEIGHT = 80;
	public static int WIDTH = 15;
	
	public PongPaddle(int x, int y, int w, int h) {
		super(x,y,w,h);
	}

	public int getW() {
		return (int) this.getWidth();
	}


	public int getH() {
		return (int) this.getHeight();
	}

}