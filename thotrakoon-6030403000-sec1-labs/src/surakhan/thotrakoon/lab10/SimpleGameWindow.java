package surakhan.thotrakoon.lab10;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;


public class SimpleGameWindow extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = -579668793285101785L;

	public static final int WIDTH = 650;
	public static final int HEIGHT = 500;
	
	public SimpleGameWindow(String string) {
		super(string);
	}
	
	protected void setFrameFeatures() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (dim.width - WIDTH)/2;
		int y = (dim.height - HEIGHT)/2;
		setLocation(x,y);
		getContentPane().setPreferredSize(new Dimension(WIDTH,HEIGHT));
		pack();
	    setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void createAndShowGUI() {
		SimpleGameWindow window = new SimpleGameWindow("My Simple Game Window");
		window.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}	
	
}