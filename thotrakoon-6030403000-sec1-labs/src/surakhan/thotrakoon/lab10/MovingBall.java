package surakhan.thotrakoon.lab10;

import surakhan.thotrakoon.lab10.Ball;

public class MovingBall extends Ball {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int velX; // velocity of the ball on x-axis
	private int velY; // velocity of the ball on y-axis
	
	//Constructor when specify the velocity
	public MovingBall(int x, int y, int r, int velX, int velY) {
		super(x, y, r);
		this.velX = velX;
		this.velY = velY;
	}

	//Constructor when not specify the velocity
	public MovingBall(int x, int y, int r) {
		super(x,y,r);
		velX = 0;
		velY = 0;
	}
	
	public int getVelX() {
		return velX;
	}

	public void setVelX(int velX) {
		this.velX = velX;
	}

	public int getVelY() {
		return velY;
	}

	public void setVelY(int velY) {
		this.velY = velY;
	}
	
	// Move the ball on x-axis according to velocity on x-axis
	public void moveX() {
		x+=velX;
	}
	
	// Move the ball on y-axis according to velocity on y-axis
	public void moveY() {
		y+=velY;
	}
	
	public void move() {
		moveX();
		moveY();
	}
	
	// return the x-coordinate of the ball if it calls moveX()
	public int chkMoveX() {
		return (int) x+velX;
	}
	
	// return the y-coordinate of the ball if it calls moveY()
	public int chkMoveY() {
		return (int) y+velY;
	}
}