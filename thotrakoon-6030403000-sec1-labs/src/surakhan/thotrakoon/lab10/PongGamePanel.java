package surakhan.thotrakoon.lab10;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

public class PongGamePanel extends JPanel {

	private static final long serialVersionUID = -8169671565580717600L;
	protected PongPaddle leftPad;
	protected PongPaddle rightPad;
	protected Integer player1Score;
	protected Integer player2Score;
	private int scoreFontSize = 48;
	protected Ball ball;
	private int ballR = 20; // radius of the ball
	protected Rectangle2D.Double box;
	
	
	public PongGamePanel() {

		setBackground(Color.BLACK);

		// initialize the pads
		leftPad = new PongPaddle(0, SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH, PongPaddle.HEIGHT);
		rightPad = new PongPaddle(SimpleGameWindow.WIDTH - PongPaddle.WIDTH, SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH, PongPaddle.HEIGHT);

		// initialize the ball
		ball = new Ball(SimpleGameWindow.WIDTH / 2 - ballR, SimpleGameWindow.HEIGHT / 2 - ballR, ballR);
		
		// rectangle box cover the whole game window.
		box = new Rectangle2D.Double(0, 0, SimpleGameWindow.WIDTH, SimpleGameWindow.HEIGHT);

		// set the player scores
		player1Score = 0;
		player2Score = 0;

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.WHITE);

		// draw the middle line
		g2.drawLine(SimpleGameWindow.WIDTH / 2, 0, SimpleGameWindow.WIDTH / 2, SimpleGameWindow.HEIGHT);

		// draw line on the left
		g2.drawLine(leftPad.getW(), 0, leftPad.getW(), SimpleGameWindow.HEIGHT);

		// draw line on the right
		g2.drawLine(SimpleGameWindow.WIDTH - rightPad.getW(), 0, SimpleGameWindow.WIDTH - rightPad.getW(), SimpleGameWindow.HEIGHT);

		// Draw the score
		g2.setFont(new Font(Font.SERIF, Font.BOLD, scoreFontSize));
		g2.drawString(player1Score.toString(), SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT/5);
		g2.drawString(player2Score.toString(), 3 * SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT/5);

		// Draw the paddles
		g2.fill(leftPad);
		g2.fill(rightPad);

		// draw the ball
		g2.fill(ball);

		// draw the box
		g2.draw(box);
	}
}